import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

sc_X = StandardScaler()
sc_Y = StandardScaler()

df = pd.read_csv("dataset.csv")

X = df[['v', 'js', 'jh']]
Y = df[['y']]

scaledX = sc_X.fit_transform(X)
scaledY = sc_Y.fit_transform(Y)

regressor = SVR(kernel='rbf')
regressor.fit(scaledX, scaledY)

y_pred = sc_Y.inverse_transform(regressor.predict(sc_X.transform(X)))

print(y_pred)
