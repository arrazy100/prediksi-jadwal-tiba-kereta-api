import tkinter as tk
from tkinter import messagebox
from tkinter import ttk

import datetime

from PIL import Image, ImageTk

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# pembuatan kelas entry hanya angka
class EntryDigit(tk.Entry):
    def __init__(self, master=None, **kwargs):
        self.var = tk.StringVar()
        tk.Entry.__init__(self, master, textvariable=self.var, **kwargs)
        self.old_value = ''
        self.var.trace('w', self.check)
        self.get, self.set = self.var.get, self.var.set

    def check(self, *args):
        if len(self.get()) > 4:
            self.set(self.get()[:-1])
        elif self.get().isdigit() or self.get() == "":
            self.old_value = self.get()
        else:
            self.set(self.old_value)

class EntryJam(tk.Entry):
	def __init__(self, master=None, **kwargs):
		self.var = tk.StringVar()
		tk.Entry.__init__(self, master, textvariable=self.var, **kwargs)
		self.old_value = ''
		self.var.trace('w', self.check)
		self.get, self.set = self.var.get, self.var.set
		self.set("00")

	def check(self, *args):
		if len(self.get()) > 2:
			self.set(self.get()[:-1])
		elif(self.get() == "-"):
			self.set("")
		elif(self.get() != "" and int(self.get()) > 23):
			self.set("23")
		elif(self.get() != "" and self.get() == "0"):
			self.set("00")
		elif self.get().isdigit() or self.get() == "":
			self.old_value = self.get()
		else:
			self.set(self.old_value)

class EntryMenit(tk.Entry):
	def __init__(self, master=None, **kwargs):
		self.var = tk.StringVar()
		tk.Entry.__init__(self, master, textvariable=self.var, **kwargs)
		self.old_value = ''
		self.var.trace('w', self.check)
		self.get, self.set = self.var.get, self.var.set
		self.set("00")

	def check(self, *args):
		if len(self.get()) > 2:
			self.set(self.get()[:-1])
		elif(self.get() == "-"):
			self.set("")
		elif(self.get() != "" and int(self.get()) > 59):
			self.set("00")
		elif(self.get() != "" and self.get() == "0"):
			self.set("00")
		elif self.get().isdigit() or self.get() == "":
			self.old_value = self.get()
		else:
			self.set(self.old_value)

# pembuatan kelas utama, yaitu kelas SistemTiket
class App(tk.Tk):
	# fungsi init atau constructor
	def __init__(self):
		tk.Tk.__init__(self)
		self.title("Prediksi Jadwal Tiba Kereta Api")  # mengubah judul window
		self.geometry('640x480')
		self.resizable(0, 0)

		# load resources
		self.load_resources()

		self.canvas = tk.Canvas(self, width=640, height=480, bg='white')
		self.canvas.pack()
		self.canvas.create_image(0, 0, anchor=tk.NW, image=self.img_bg)

		self.entry_keberangkatan_jam = EntryJam(self, width=2)
		self.entry_keberangkatan_menit = EntryMenit(self, width=2)
		self.entry_kecepatan = EntryDigit(self, width=4)
		self.entry_jumlah_stasiun_pemberhentian = EntryDigit(self, width=4)
		self.entry_jumlah_hambatan = EntryDigit(self, width=4)
		self.entry_jarak = EntryDigit(self, width=4)

		self.canvas.create_text(150, 115, anchor='nw', fill='white', text="Keberangkatan", font="Arial 10")
		self.canvas.create_text(150, 145, anchor='nw', fill='white', text="Kecepatan Kereta Api", font="Arial 10")
		w = self.canvas.create_text(150, 175, anchor='nw', fill='white', text="Jumlah Stasiun Pemberhentian", font="Arial 10")
		self.canvas.create_text(150, 205, anchor='nw', fill='white', text="Jarak Tempuh Kereta", font="Arial 10")
		self.pred_l = self.canvas.create_text(150, 380, anchor='nw', fill='white', text="Hasil Prediksi Kereta Tiba Pukul", font="Arial 10")
		self.canvas.create_rectangle(130, 365, 450, 390 + self.get_height(self.pred_l))

		w2 = self.add(self.entry_keberangkatan_jam, self.get_width(w) + 170, 110)
		self.canvas.create_text(self.get_width(w2) + self.get_width(w) + 173, 112, anchor='nw', fill='white', text=":", font="Arial 10")
		self.add(self.entry_keberangkatan_menit, self.get_width(w) + self.get_width(w2) + 180, 110)
		self.add(self.entry_kecepatan, self.get_width(w) + 170, 140)
		self.add(self.entry_jumlah_stasiun_pemberhentian, self.get_width(w) + 170, 170)
		self.add(self.entry_jarak, self.get_width(w) + 170, 200)

		btn_prediksi = tk.Button(self, text="Prediksi", command=self.prediksi, bg='green', foreground='white', activebackground='red', activeforeground='white')
		prediksi_w = self.add(btn_prediksi, 400, 270)

		btn_reset = tk.Button(self, text="Reset", command=self.reset, bg='green', foreground='white', activebackground='red', activeforeground='white')
		self.add(btn_reset, self.get_width(prediksi_w) + 410, 270)

	def load_resources(self):
		self.pil_bg = Image.open("bg_image.jpg").resize((640, 480), Image.ANTIALIAS)
		self.img_bg = ImageTk.PhotoImage(self.pil_bg)

		self.sc_X = StandardScaler()
		self.sc_Y = StandardScaler()

		df = pd.read_csv("dataset.csv")

		X = df[['Kecepatan', 'Jumlah Stasiun', 'Jarak']]
		Y = df[['waktu']]

		scaledX = self.sc_X.fit_transform(X)
		scaledY = self.sc_Y.fit_transform(Y)

		self.regressor = SVR(kernel='linear')
		self.regressor.fit(scaledX, scaledY)

	def add(self, widget, x, y):
		canvas_window = self.canvas.create_window(
			x, y, anchor=tk.NW, window=widget)
		return canvas_window

	def get_width(self, widget):
		bound = self.canvas.bbox(widget)
		return bound[2] - bound[0]

	def get_height(self, widget):
		bound = self.canvas.bbox(widget)
		return bound[3] - bound[1]

	def prediksi(self):
		if (self.entry_kecepatan.get() == "" or
			self.entry_jumlah_stasiun_pemberhentian.get() == "" or
			self.entry_jarak.get() == ""):
			messagebox.showwarning("Gagal", "Pastikan form masukkan telah terisi semua")
			return

		if (int(self.entry_kecepatan.get()) < 50 or int(self.entry_kecepatan.get()) > 80):
			messagebox.showwarning("Gagal", "Minimal kecepatan kereta api adalah 50 Km/ jam dan maksimal 80 Km/ jam")
			return

		X = [[
				int(self.entry_kecepatan.get()),
				int(self.entry_jumlah_stasiun_pemberhentian.get()),
				int(self.entry_jarak.get())
			]]

		y_pred = self.sc_Y.inverse_transform(self.regressor.predict(self.sc_X.transform(X)))
		y = int(round(y_pred[0]))
		time = datetime.datetime.now().replace(hour=int(self.entry_keberangkatan_jam.get()),
							minute=int(self.entry_keberangkatan_menit.get())) + datetime.timedelta(minutes=y)
		self.canvas.itemconfigure(self.pred_l, text="Hasil Prediksi Kereta Tiba Pukul " + time.strftime('%H : %M'))

	def reset(self):
		self.entry_keberangkatan_jam.set("00")
		self.entry_keberangkatan_menit.set("00")
		self.entry_kecepatan.delete(0, 'end')
		self.entry_jumlah_stasiun_pemberhentian.delete(0, 'end')
		self.entry_jumlah_hambatan.delete(0, 'end')
		self.entry_jarak.delete(0, 'end')
		self.canvas.itemconfigure(self.pred_l, text="Hasil Prediksi Kereta Tiba Pukul ")

if __name__ == '__main__':
	app = App()
	app.mainloop()
